package com.javan.tugas2;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class GanjilGenapController {

    List<DataAngka> listHasil = new ArrayList<>();;
    @GetMapping("/ganjilgenap")
    public String getGanjilGenap(Model model) {

        model.addAttribute("angka", 0);
        model.addAttribute("hasil", "genap");
        model.addAttribute("angkaKedua", 0);
        model.addAttribute("hasilKedua", "genap");
        return "ganjilgenap";
    }

    @PostMapping("/ganjilgenap")
    public String postGanjilGenap(@RequestParam(value = "angka", defaultValue = "0") int angka,
                                  @RequestParam(value = "angkaKedua", defaultValue = "0") int angkaKedua,
                                  Model model) {

        if( angka > angkaKedua){
            model.addAttribute("error", String.format("Angka %d lebih dari %d, operasi tidak dapat dilakukan", angka, angkaKedua));
        }
        else{
            listHasil.clear();
            model.addAttribute("error", "tidak");
            for(int i = angka; i <= angkaKedua; i++){
                if(i%2 == 0){
                    listHasil.add(new DataAngka(i, "genap"));
                }
                else
                    listHasil.add(new DataAngka(i, "ganjil"));
            }
            model.addAttribute("listHasil", listHasil);
        }
        model.addAttribute("angka", angka);
        model.addAttribute("angkaKedua", angkaKedua);


        return "ganjilgenap";
    }
}
