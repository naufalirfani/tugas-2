package com.javan.tugas2;

public class KalkulatorData {
    private int angkaPertama;
    private String operator;
    private int angkaKedua;

    public KalkulatorData(){

    }

    public int getAngkaPertama(){
        return angkaPertama;
    }

    public String getOperator(){
        return operator;
    }

    public int getAngkaKedua(){
        return angkaKedua;
    }

    public void setAngkaPertama(int angkaPertama){
        this.angkaPertama = angkaPertama;
    }

    public void setOperator(String operator){
        this.operator = operator;
    }

    public void setAngkaKedua(int angkaKedua){
        this.angkaKedua = angkaKedua;
    }
}
