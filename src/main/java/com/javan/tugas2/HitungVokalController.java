package com.javan.tugas2;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

@Controller
public class HitungVokalController {
    private static final ArrayList<Character> listChar = new ArrayList<>();
    @GetMapping("/hitungvokal")
    public String getVokal(Model model) {

        model.addAttribute("text", "");
        model.addAttribute("hasil", "Harap masukkan text");
        return "hitungvokal";
    }

    @PostMapping("/hitungvokal")
    public String postVokal(@RequestParam(value = "text", defaultValue = "") String text, Model model) {

        listChar.clear();
        String hasil;

        for(int j = 0; j < text.length(); j++){
            text = text.toLowerCase();
            char ch = text.charAt(j);
            if(ch == 'a' || ch == 'i' || ch == 'u' || ch == 'e' || ch == 'o'){
                if(!listChar.contains(ch)) {
                    listChar.add(ch);
                }
            }
        }

        if(listChar.size() == 0){
           hasil = String.format("\"%s\" = Tidak ada huruf vokal\n", text);
        }
        else if(listChar.size() == 1){
            hasil = String.format("\"%s\" = %d yaitu %c\n",
                    text,
                    listChar.size(),
                    listChar.get(0));
        }
        else if(listChar.size() == 2){
            hasil = String.format("\"%s\" = %d yaitu %c dan %c\n",
                    text,
                    listChar.size(),
                    listChar.get(0),
                    listChar.get(1));
        }
        else if(listChar.size() == 3){
            hasil = String.format("\"%s\" = %d yaitu %c, %c, dan %c\n",
                    text,
                    listChar.size(),
                    listChar.get(0),
                    listChar.get(1),
                    listChar.get(2));
        }
        else if(listChar.size() == 4){
            hasil = String.format("\"%s\" = %d yaitu %c, %c, %c, dan %c\n",
                    text,
                    listChar.size(),
                    listChar.get(0),
                    listChar.get(1),
                    listChar.get(2),
                    listChar.get(3));
        }
        else if(listChar.size() == 5){
            hasil = String.format("\"%s\" = %d yaitu %c, %c, %c, %c, dan %c\n",
                    text,
                    listChar.size(),
                    listChar.get(0),
                    listChar.get(1),
                    listChar.get(2),
                    listChar.get(3),
                    listChar.get(4));
        }
        else{
            hasil = "Harap masukkan text";
        }

        model.addAttribute("hasil", hasil);

        return "hitungvokal";
    }
}
