package com.javan.tugas2;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class KalkulatorController {

    @GetMapping("/kalkulator")
    public String kalkulatorForm(Model model) {
        model.addAttribute("kalkulatorData", new KalkulatorData());
        return "kalkulator";
    }

    @PostMapping("/kalkulator")
    public String kalkulatorSubmit(@ModelAttribute KalkulatorData kalkulatorData,
                                 @RequestParam(value = "yourButton") String operator,
                                 Model model) {
        model.addAttribute("kalkulatorData", kalkulatorData);
        model.addAttribute("operator", operator);

        int hasil;
        switch (operator) {
            case "+":
                hasil = kalkulatorData.getAngkaPertama() + kalkulatorData.getAngkaKedua();
                model.addAttribute("hasil", hasil);
                break;
            case "-":
                hasil = kalkulatorData.getAngkaPertama() - kalkulatorData.getAngkaKedua();
                model.addAttribute("hasil", hasil);
                break;
            case "x":
                hasil = kalkulatorData.getAngkaPertama() * kalkulatorData.getAngkaKedua();
                model.addAttribute("hasil", hasil);
                break;
            case "/":
                try {
                    float hasilBagi = kalkulatorData.getAngkaPertama() / (float) kalkulatorData.getAngkaKedua();
                    model.addAttribute("hasil", hasilBagi);
                } catch (Exception e) {
                    model.addAttribute("hasil", "Tidak bisa dilakukan");
                }
                break;
        }

        return "result";
    }

}
